import edu.princeton.cs.algs4.WeightedQuickUnionUF;

//run java thru commandline. ex) java-algs4 PercolationStats 2 10000

public class Percolation {

    private int[][] id;
    private WeightedQuickUnionUF UF;
    private int closed_sites;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if (n <= 0)
            throw new IllegalArgumentException("n must be <= 0");

        this.id = new int[n][n]; //row,col initializes to all zero
        this.UF = new WeightedQuickUnionUF(n * n + 2); //create UF object with 2 virtual sites
        this.closed_sites = n * n;
        //the first n elements of our nxn grid will be unioned with VS1
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        boolean a = row > this.id.length;
        boolean b = row < 0;
        boolean c = col > this.id.length;
        boolean d = col < 0;

        if (a && b && c && d)
            throw new IllegalArgumentException("Row and/or Columns is not is required range");

        //UI demands that 1,1 is first node... ex. if 1,1 is given, they mean 0,0 in id array
        if (this.id[row - 1][col - 1] != 1) {
            this.id[row - 1][col - 1] = 1;
            this.closed_sites -= 1;

            if (row == this.id.length)
                this.UF.union(this.id.length * (row - 1) + col, this.id.length * this.id.length + 1);
            if (row == 1)
                this.UF.union(this.id.length * (row - 1) + col, 0);


            if (row - 1 > 0)
                if (isOpen(row - 1, col)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row - 2) + col);
                }
            if (row + 1 <= this.id.length)
                if (isOpen(row + 1, col)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row) + col);
                }

            if (col + 1 <= this.id.length)
                if (isOpen(row, col + 1)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row - 1) + col + 1);
                }
            if (col - 1 > 0)
                if (isOpen(row, col - 1)) {
                    this.UF.union(this.id.length * (row - 1) + col, this.id.length * (row - 1) + col - 1);
                }
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        boolean a = row > this.id.length;
        boolean b = row < 0;
        boolean c = col > this.id.length;
        boolean d = col < 0;

        if (a && b && c && d)
            throw new IllegalArgumentException("Row and/or Columns is not is required range");
        return this.id[row - 1][col - 1] == 1;
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        boolean a = row > this.id.length;
        boolean b = row < 0;
        boolean c = col > this.id.length;
        boolean d = col < 0;

        if (a && b && c && d)
            throw new IllegalArgumentException("Row and/or Columns is not is required range");
        if (isOpen(row, col)) {
            if (this.UF.find(this.id.length * (row - 1) + col) == this.UF.find(0)) { //canonical elements of given site is equal to canonical of VS1
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return this.id.length * this.id.length - this.closed_sites;
    }

    // does the system percolate?
    public boolean percolates() {
        return (this.UF.find(this.id.length * this.id.length + 1) == this.UF.find(0));
    }

    // test client (optional)
    public static void main(String[] args) {


    }
}
