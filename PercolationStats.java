import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    // perform independent trials on an n-by-n grid
    private double[] perc_threshold;

    public PercolationStats(int n, int trials) {
        this.perc_threshold = new double[trials];
        for (int i = 0; i < trials; i++) {
            Percolation sim = new Percolation(n);
            while (!sim.percolates()) {

                int row = StdRandom.uniform(1, n + 1);
                int col = StdRandom.uniform(1, n + 1);
                if (!sim.isOpen(row, col))
                    sim.open(row, col);
            }

            this.perc_threshold[i] = sim.numberOfOpenSites() / (double) (n * n);
        }

    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(this.perc_threshold);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(this.perc_threshold);
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        return mean() - (1.96 * stddev() / Math.sqrt(this.perc_threshold.length));

    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return mean() + (1.96 * stddev() / Math.sqrt(this.perc_threshold.length));
    }

    // test client (see below)
    public static void main(String[] args) {
        PercolationStats Monte_Carlo = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println("mean: " + (Monte_Carlo.mean()));
        System.out.println("stddev: " + (Monte_Carlo.stddev()));
        System.out.println("conf_interval: " + "[" + (Monte_Carlo.confidenceHi()) + " , " + (Monte_Carlo.confidenceLo()) + "]");
    }

}
